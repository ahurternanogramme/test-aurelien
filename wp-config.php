<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'test-aurelien-pitaut.loc' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '3,!~yj)yq30i.7u*9y?<nXvp6*-k?g)}t@WV[i{6|OUFT_d=n2,p(!DT1BCWux_^' );
define( 'SECURE_AUTH_KEY',  'WtTZbhy1gvKxReyoo:}hzN7:xFT*+)xtXSuAm(6!1?/f<:24svk5fDeQfwU7r=y ' );
define( 'LOGGED_IN_KEY',    '6Lh8~%]p 5YY!RPiYqn|Bl4!G,?xYtC0-_GjT:7(MEl./K]f^ r8+NphIIcI-*-#' );
define( 'NONCE_KEY',        '~gPf2koiL>]WwSy]hmh{SulGB,Xq@];g0_8um.F,k+GqT,w;@idwMTs)*6U <2XF' );
define( 'AUTH_SALT',        'j-,$e&2nXs(!d&Il3I;=g4|Y+08pGNV*zTMM)B@:$gjEZ6=4[LO#vSOy4bRhb{e>' );
define( 'SECURE_AUTH_SALT', 'CHQ]F>:$}N#hPu+9gTz}dd6U>|XWMKS^ZXzg>|Q@^J|Mzposzi_+{CQ~q!DxJ/wk' );
define( 'LOGGED_IN_SALT',   'w}n)y;/luX{PhJb%H;0C02?xTM#if}GbS;mBj]PMQSu~[4!tMu:#&{H}8-v<lCQ{' );
define( 'NONCE_SALT',       'j=j0YRO3MG*gG< /+=vA^Uy<N]i@6|8#*P&0(G@j9G|M?TJgo|}vtz1v#E>*?c1M' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
