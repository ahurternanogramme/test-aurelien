<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package test-aurelien-pitaut
 */

?>

<footer id="colophon" class="site-footer">
	<div>
		<?php
		the_custom_logo();
		?>
		<?php
		wp_nav_menu(
			array(
				'theme_location' => 'menu-2',
				'menu_id'        => 'secondary-menu',
			)
		);
		?>
	</div>
	<div id="copyright">
		<?php
		wp_nav_menu(
			array(
				'theme_location' => 'menu-3',
				'menu_id'        => 'tertiary-menu',
			)
		);
		?>
	</div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>